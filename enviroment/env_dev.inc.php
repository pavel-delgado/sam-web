<?php

/*
 * NICHER CL 2017
 * PAVEL DELGADO / p.delgado@nicher.cl
 *
 *  SAM ENVIROMENT FILE
 */

// URL SETTINGS
define("BASE_FQDN", '//localhost:8000');
define("BASE_URL", '');

// DATABASE
define("DB_HOST", "nicher-cl.cwkhugldfcbk.sa-east-1.rds.amazonaws.com");
define("DB_NAME", "malbec");
define("DB_USER", "malbec");
define("DB_SECRET", "malbec123");

// MANEJO DE ERRORES
//  0 |-1 => NINGUNO | TODOS
error_reporting(-1);

// SET LOCALE CHILE
setlocale(LC_ALL, "es_CL.UTF-8");

// MAIL

//$mail->isSMTP();
//$mail->Host = '';
//$mail->SMTPAuth = true;
//$mail->Username = '';
//$mail->Password = '';
//$mail->SMTPSecure = 'tls';
//$mail->Port = 587;
//
//$mail->setFrom('noresponder@sic.cl', 'SIC WEB');
