<?php

$router->get('/', ['Controller\\BASEController', "pageLogin"]);

$router->filter('loggedFilter', ['Controller\\BASEController', "filterLogged"]);

$router->group(['before' => 'loggedFilter', 'prefix' => 'panel'], function($router) {
    $router->get('', ['Controller\\basePanelController', "pageHome"]);
    $router->get('/logout', ['Controller\\BASEController', "pageLogout"]);

    // CLIENT CONTROLLER
    $router->get('/clientes', ['Controller\\panelClientController', "pageClient"]);

    //BRANCH CONTROLLER
    $router->get('/cliente/{client}/sucursales', ['Controller\\clientBranchController', "pageBranch"]);

    // EQUIPMENT CONTROLLER
    $router->get('/cliente/{client}/sucursal/{branch}/equipos', ['Controller\\branchEquipmentController', "pageEquipment"]);

    // EQUIPMENT REVISION CONTROLLER
    $router->get('/cliente/{client}/sucursal/{branch}/equipo/{equipment}/revisiones', ['Controller\\equipmentRevisionController', 'pageRevisionEquipment']);

    // VISIT CONTROLLER
    $router->get('/visitas', ['Controller\\panelVisitController', 'pageVisit']);

    // VISIT REVISION CONTROLLER
    $router->get('/visita/{visit}/revisiones', ['Controller\\visitRevisionController', 'pageVisitRevisions']);

    // TECHNICIAN CONTROLLER
    $router->get('/tecnicos', ['Controller\\panelTechnicianController', 'pageTechnician']);
});

$router->group(['prefix' => 'ajax'], function($router) {
    $router->post('/login', ['Controller\\BASEController', "ajaxLogin"]);

    // CLIENT CONTROLLER
    $router->post('/cliente/create', ['Controller\\panelClientController', "ajaxCreateClient"]);
    $router->post('/cliente/delete', ['Controller\\panelClientController', "ajaxDeleteClient"]);

    //BRANCH CONTROLLER
    $router->post('/sucursal/create', ['Controller\\clientBranchController', "ajaxCreateBranch"]);
    $router->post('/sucursal/delete', ['Controller\\clientBranchController', "ajaxDeleteBranch"]);
    $router->post('/sucursal/encargados', ['Controller\\clientBranchController', "ajaxAttendants"]);
    $router->post('/sucursal/createEncargado', ['Controller\\clientBranchController', "ajaxCreateAttendant"]);

    // EQUIPMENT CONTROLLER
    $router->post('/equipo/create', ['Controller\\branchEquipmentController', "ajaxCreateEquipment"]);
    $router->post('/equipo/delete', ['Controller\\branchEquipmentController', "ajaxDeleteEquipment"]);
    $router->post('/equipo/details', ['Controller\\branchEquipmentController', "ajaxDetails"]);

    // VISIT CONTROLLER
    $router->post('/visita/create', ['Controller\\panelVisitController', "ajaxCreateVisit"]);
    $router->post('/visita/delete', ['Controller\\panelVisitController', "ajaxDeleteVisit"]);
    $router->post('/visita/branches', ['Controller\\panelVisitController', "ajaxGetBranches"]);
    $router->post('/visita/technicians', ['Controller\\panelVisitController', "ajaxGetTechnicians"]);

    // TECHNICIAN CONTROLLER
    $router->post('/tecnico/create', ['Controller\\panelTechnicianController', "ajaxCreateTechnician"]);
    $router->post('/tecnico/delete', ['Controller\\panelTechnicianController', "ajaxDeleteTechnician"]);
});


$router->group(['prefix' => 'api'], function($router) {
    $router->post('/login', ['Controller\\APIController', "apiLogin"]);
    $router->post('/visit', ['Controller\\APIController', "apiVisit"]);
    $router->post('/equipment', ['Controller\\APIController', "apiEquipmentfromBranch"]);
    $router->post('/equipment/revision/read', ['Controller\\APIController', "apiRevisionfromEquipment"]);
    $router->post('/equipment/revision/write', ['Controller\\APIController', "apiRevisiontoEquipment"]);
});
