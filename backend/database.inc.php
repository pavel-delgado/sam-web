<?php

/*
 * NICHER CL 2016
 * PAVEL DELGADO / p.delgado@nicher.cl
 *
 *  SUFRAG
 */

// DBAL DOCTRINE

$doctrineConfig = new \Doctrine\DBAL\Configuration();

$databaseParams = array(
    'dbname' => DB_NAME,
    'user' => DB_USER,
    'password' => DB_SECRET,
    'host' => DB_HOST,
    'driver' => 'pdo_pgsql',
);
