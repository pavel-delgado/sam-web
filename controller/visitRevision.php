<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

class visitRevisionController {

    function pageVisitRevisions($visit_id) {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query_visit = $database->createQueryBuilder();
        $query_visit
                ->select('vis.visita_id', 'vis.visita_fecha', 'suc.sucursal_nombre')
                ->from('visita', 'vis')
                ->leftJoin('vis', 'sucursal', 'suc', 'vis.sucursal_id = suc.sucursal_id')
                ->where('vis.visita_id = :visita_id')
                ->setParameter('visita_id', $visit_id)
        ;
        $visit = $query_visit->execute()->fetch();

        $query_revisions = $database->createQueryBuilder();
        $query_revisions
                ->select('rev.equipo_id', 'rev.revision_tipo', 'rev.revision_notas', 'suc.sucursal_nombre', 'equ.equipo_nombre')
                ->from('revision', 'rev')
                ->leftJoin('rev', 'equipo', 'equ', 'rev.equipo_id = equ.equipo_id')
                ->leftJoin('equ', 'sucursal', 'suc', 'equ.sucursal_id = suc.sucursal_id')
                ->where('rev.visita_id = :visita_id')
                ->setParameter('visita_id', $visit_id)
        ;
        $revisions = $query_revisions->execute()->fetchAll();
        return $template->render('visitRevision.twig', [
                    'BASE_URL' => BASE_FQDN . BASE_URL,
                    'revisions' => $revisions,
                    'visit' => $visit
        ]);
    }

}
