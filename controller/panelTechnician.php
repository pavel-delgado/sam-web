<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of panelTechnicianController
 *
 * @author Eduardo
 */
class panelTechnicianController {

    function pageTechnician() {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('tecnico_id', 'tecnico_nombre','tecnico_apellido','tecnico_rol')
                ->from('malbec.tecnico')
        ;
        $technicians = $query->execute()->fetchAll();
        return $template->render('panelTechnician.twig', ['BASE_URL' => BASE_FQDN . BASE_URL, 'technicians' => $technicians]);
    }

    function ajaxCreateTechnician() {
        $tecnico_nombre = filter_input(INPUT_POST, 'tecnico_nombre', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $tecnico_apellido = filter_input(INPUT_POST, 'tecnico_apellido', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $tecnico_id = filter_input(INPUT_POST, 'tecnico_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $tecnico_contrasena = filter_input(INPUT_POST, 'tecnico_contrasena', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $tecnico_rol = filter_input(INPUT_POST, 'tecnico_rol', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.tecnico')
                ->values(array(
                    'tecnico_nombre' => ':tecnico_nombre',
                    'tecnico_apellido' => ':tecnico_apellido',
                    'tecnico_id' => ':tecnico_id',
                    'tecnico_contrasena' => ':tecnico_contrasena',
                    'tecnico_rol' => ':tecnico_rol'
                ))
                ->setParameter('tecnico_nombre', $tecnico_nombre)
                ->setParameter('tecnico_apellido', $tecnico_apellido)
                ->setParameter('tecnico_id', $tecnico_id)
                ->setParameter('tecnico_contrasena', sha1($tecnico_contrasena))
                ->setParameter('tecnico_rol', $tecnico_rol)
        ;

        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(409);
        }
    }
    function deleteTechnician($tecnico_id) {
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->delete('malbec.tecnico')
                ->where('tecnico_id = :tecnico_id')
                ->setParameter('tecnico_id', $tecnico_id)
        ;
        try {
            $query->execute();
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }
            
    function ajaxDeleteTechnician() {
        $tecnico_id = filter_input(INPUT_POST, 'tecnico_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        if (!panelTechnicianController::deleteTechnician($tecnico_id)) {
            \http_response_code(404);
        }
    }
}
