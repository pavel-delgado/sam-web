<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 */

namespace Controller;

/**
 * Description of backend
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
class BASEController {

    function pageLogin() {
        global $template;
        return $template->render('baseLogin.twig', ['BASE_URL' => BASE_FQDN . BASE_URL]);
    }

    function ajaxLogin() {
        $user = trim(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $pass = trim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('administrador_contrasena', 'administrador_id')
                ->from('malbec.administrador')
                ->where('administrador_nombre=?')
                ->setParameter(0, $user)
        ;
        $response = array();
        if ($query->execute()->fetchColumn(0) == sha1($pass)) {
            $response["login_status"] = 'success';
            $response["redirect_url"] = '/panel';
            session_start();
            $_SESSION['id'] = $query->execute()->fetchColumn(1);
            header('Content-type: application/json; charset=utf-8');
            return json_encode($response);
        } else {
            $response["login_status"] = 'invalid';
            session_start();
            header('Content-type: application/json; charset=utf-8');
            return json_encode($response);
        }
    }

    function pageLogout() {
        $_SESSION = array();
        session_destroy();
        session_unset();
        header('Location:/login');
    }

    function filterLogged() {
        session_start();
        if (!isset($_SESSION['id'])) {
            header('Location: /');
            return false;
        }
    }

}
