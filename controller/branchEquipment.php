<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of branchController
 *
 * @author Jean Paul Astudillo
 */
class branchEquipmentController {

    public function pageEquipment($client_id, $branch_id) {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query_branch = $database->createQueryBuilder();
        $query_branch
                ->select('suc.sucursal_id', 'suc.sucursal_nombre', 'emp.empresa_id', 'emp.empresa_nombre')
                ->from('sucursal', 'suc')
                ->where('sucursal_id=?')
                ->leftJoin('suc', 'empresa', 'emp', 'suc.empresa_id = emp.empresa_id')
                ->setParameter(0, $branch_id)
        ;

        $branch = $query_branch->execute()->fetch();

        $query_equipments = $database->createQueryBuilder();
        $query_equipments
                ->select('*')
                ->from('malbec.equipo')
                ->where('sucursal_id=?')
                ->setParameter(0, $branch_id)
        ;
        $equipments = $query_equipments->execute()->fetchAll();

        return $template->render('branchEquipment.twig', [
                    'BASE_URL' => BASE_FQDN . BASE_URL,
                    'equipments' => $equipments,
                    'branch' => $branch
        ]);
    }

    public function ajaxCreateEquipment() {

        require __DIR__ . '/../backend/database.inc.php';

        $equipo_id = date("dmYHisL");
        $sucursal_id = filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_nombre = filter_input(INPUT_POST, 'equipo_nombre', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_tipo = filter_input(INPUT_POST, 'equipo_tipo', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_marca = filter_input(INPUT_POST, 'equipo_marca', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_modelo = filter_input(INPUT_POST, 'equipo_modelo', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_serie = filter_input(INPUT_POST, 'equipo_serie', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_ubicacion = filter_input(INPUT_POST, 'equipo_ubicacion', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_piso = filter_input(INPUT_POST, 'equipo_piso', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $equipo_periodicidad = filter_input(INPUT_POST, 'equipo_periodicidad', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.equipo')
                ->values(array(
                    'equipo_id' => ':equipo_id',
                    'sucursal_id' => ':sucursal_id',
                    'equipo_nombre' => ':equipo_nombre',
                    'equipo_tipo' => ':equipo_tipo',
                    'equipo_marca' => ':equipo_marca',
                    'equipo_modelo' => ':equipo_modelo',
                    'equipo_serie' => ':equipo_serie',
                    'equipo_ubicacion' => ':equipo_ubicacion',
                    'equipo_piso' => ':equipo_piso',
                    'equipo_periodicidad' => ':equipo_periodicidad'
                ))
                ->setParameter('equipo_id', $equipo_id)
                ->setParameter('sucursal_id', $sucursal_id)
                ->setParameter('equipo_nombre', $equipo_nombre)
                ->setParameter('equipo_tipo', $equipo_tipo)
                ->setParameter('equipo_marca', $equipo_marca)
                ->setParameter('equipo_modelo', $equipo_modelo)
                ->setParameter('equipo_serie', $equipo_serie)
                ->setParameter('equipo_ubicacion', $equipo_ubicacion)
                ->setParameter('equipo_piso', $equipo_piso)
                ->setParameter('equipo_periodicidad', $equipo_periodicidad)
        ;

        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(409);
        }
    }

    public function ajaxDeleteEquipment() {
        $equipo_id = filter_input(INPUT_POST, 'equipo_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query1 = $database->createQueryBuilder();

        $query1
                ->delete('malbec.revision')
                ->where('equipo_id = :equipo_id')
                ->setParameter('equipo_id', $equipo_id)
        ;
        $query
                ->delete('malbec.equipo')
                ->where('equipo_id = :equipo_id')
                ->setParameter('equipo_id', $equipo_id)
        ;

        try {
            $query1->execute();
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(404);
        }
    }

}
