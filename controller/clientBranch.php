<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of branchController
 *
 * @author Eduardo
 */
class clientBranchController {

    public function pageBranch($client_id) {
        global $template;

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query_client = $database->createQueryBuilder();
        $query_client
                ->select('*')
                ->from('empresa', 'emp')
                ->where('empresa_id=?')
                ->setParameter(0, $client_id)
        ;

        $client = $query_client->execute()->fetch();

        $query_branches = $database->createQueryBuilder();
        $query_branches
                ->select('*')
                ->from('sucursal', 'suc')
                ->where('empresa_id=?')
                ->setParameter(0, $client_id)
        ;
        $branches = $query_branches->execute()->fetchAll();

        return $template->render('clientBranch.twig', [
                    'BASE_URL' => BASE_FQDN . BASE_URL,
                    'branches' => $branches,
                    'client' => $client]);
    }

    public function ajaxCreateBranch() {

        require __DIR__ . '/../backend/database.inc.php';

        $empresa_id = filter_input(INPUT_POST, 'empresa_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $sucursal_id = filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $sucursal_nombre = filter_input(INPUT_POST, 'sucursal_nombre', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $sucursal_comuna = filter_input(INPUT_POST, 'sucursal_comuna', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $sucursal_direccion = filter_input(INPUT_POST, 'sucursal_direccion', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.sucursal')
                ->values(array(
                    'sucursal_id' => ':sucursal_id',
                    'empresa_id' => ':empresa_id',
                    'sucursal_nombre' => ':sucursal_nombre',
                    'sucursal_comuna' => ':sucursal_comuna',
                    'sucursal_direccion' => ':sucursal_direccion'
                ))
                ->setParameter('sucursal_id', $sucursal_id)
                ->setParameter('empresa_id', $empresa_id)
                ->setParameter('sucursal_nombre', $sucursal_nombre)
                ->setParameter('sucursal_comuna', $sucursal_comuna)
                ->setParameter('sucursal_direccion', $sucursal_direccion)
        ;

        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(409);
        }
    }

    public function ajaxDeleteBranch() {
        $sucursal_id = filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query1 = $database->createQueryBuilder();
        $query2 = $database->createQueryBuilder();
        $query3 = $database->createQueryBuilder();
        $query4 = $database->createQueryBuilder();

        $query1
                ->delete('malbec.revision')
                ->where('equipo_id = "SELECT equipo_id
                                        FROM equipo
                                        where sucursal_id = :sucursal_id"')
                ->setParameter('sucursal_id', $sucursal_id)
        ;
        $query2
                ->delete('malbec.equipo')
                ->where('sucursal_id = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id)
        ;

        $query3
                ->delete('malbec.visita')
                ->where('sucursal_id = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id)
        ;

        $query4
                ->delete('malbec.sucursal')
                ->where('sucursal_id = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id)
        ;
        try {
            $query1->execute();
            $query2->execute();
            $query3->execute();
            $query4->execute();
        } catch (\Exception $e) {
            http_response_code(404);
        }
    }

    public function asd() {
        require __DIR__ . '/../backend/database.inc.php';

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->delete('malbec.sucursal')
                ->where('sucursal_id = :sucursal_id')
                ->setParameter('sucursal_id', "asdasdasd")
        ;


        return var_dump($query->execute());
    }

    public function ajaxAttendants() {
        require __DIR__ . '/../backend/database.inc.php';
        $sucursal_id = filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('tabla.encargados,en.encargado_tipo,en.encargado_nombre,en.encargado_apellido')
                ->from('sucursal', 'suc')
                ->leftJoin('suc', '(SELECT malbec.sucursal.sucursal_id, UNNEST (malbec.sucursal.sucursal_encargados) AS encargados FROM malbec.sucursal)', 'tabla', 'suc.sucursal_id=tabla.sucursal_id')
                ->leftJoin('suc', 'encargado', 'en', 'tabla.encargados=en.encargado_id')
                ->where('suc.sucursal_id = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id)
        ;
        return json_encode($query->execute()->fetchAll());
    }

    public function ajaxCreateAttendant() {

        require __DIR__ . '/../backend/database.inc.php';

        $encargado_id = filter_input(INPUT_POST, 'encargado_id', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $encargado_email = filter_input(INPUT_POST, 'encargado_email', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $encargado_telefono = filter_input(INPUT_POST, 'encargado_telefono', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $encargado_tipo = filter_input(INPUT_POST, 'encargado_tipo', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $encargado_nombre = filter_input(INPUT_POST, 'encargado_nombre', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $encargado_apellido = filter_input(INPUT_POST, 'encargado_apellido', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);

        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.encargado')
                ->values(array(
                    'encargado_id' => ':encargado_id',
                    'encargado_nombre' => ':encargado_nombre',
                    'encargado_apellido' => ':encargado_apellido',
                    'encargado_email' => ':encargado_email',
                    'encargado_telefono' => ':encargado_telefono'
                ))
                ->setParameter('encargado_id', $encargado_id)
                ->setParameter('encargado_nombre', $encargado_nombre)
                ->setParameter('encargado_apellido', $encargado_apellido)
                ->setParameter('encargado_email', $encargado_email)
                ->setParameter('encargado_telefono', $encargado_telefono)
                ->setParameter('encargado_tipo', $encargado_tipo)
        ;
        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(409);
        }
    }

}
