<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

class equipmentRevisionController {

    function pageRevisionEquipment($client_id, $branch_id, $equipment_id) {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query_equipment = $database->createQueryBuilder();
        $query_equipment
                ->select('equ.equipo_id', 'equ.equipo_nombre', 'suc.sucursal_id', 'suc.sucursal_nombre', 'emp.empresa_id', 'emp.empresa_nombre')
                ->from('equipo', 'equ')
                ->where('equipo_id = :equipo_id')
                ->leftJoin('equ', 'sucursal', 'suc', 'equ.sucursal_id = suc.sucursal_id')
                ->leftJoin('suc', 'empresa', 'emp', 'suc.empresa_id = emp.empresa_id')
                ->setParameter('equipo_id', $equipment_id)
        ;
        $equipment = $query_equipment->execute()->fetch();

        $query_revisions = $database->createQueryBuilder();
        $query_revisions
                ->select('rev.equipo_id', 'rev.revision_tipo', 'rev.revision_notas', 'rev.revision_tipo', 'vis.visita_fecha')
                ->from('revision', 'rev')
                ->where('equipo_id = :equipo_id')
                ->leftJoin('rev', 'visita', 'vis', 'rev.visita_id = vis.visita_id')
                ->setParameter('equipo_id', $equipment_id)
        ;
        $revisions = $query_revisions->execute()->fetchAll();

        return $template->render('equipmentRevision.twig', [
                    'BASE_URL' => BASE_FQDN . BASE_URL,
                    'revisions' => $revisions,
                    'equipment' => $equipment
        ]);
    }

}
