<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of panelVisitController
 *
 * @author Eduardo
 */
class panelVisitController {

    public function pageVisit() {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('vis.visita_id', 'vis.sucursal_id', 'suc.sucursal_nombre', 'emp.empresa_nombre', 'vis.visita_fecha', 'vis.visita_estado', 'count(*) as nrev', 'nombre_tecnicos')
                ->from('visita', 'vis')
                ->leftJoin('vis', 'sucursal', 'suc', 'vis.sucursal_id = suc.sucursal_id')
                ->leftJoin('vis', 'empresa', 'emp', 'suc.empresa_id = emp.empresa_id')
                ->leftJoin('vis', 'revision', 'rev', 'vis.visita_id = rev.visita_id')
                ->leftJoin('vis', "( SELECT string_agg ( concat ( tecnico_nombre, ' ', tecnico_apellido ), ', ' ) AS nombre_tecnicos, vis_id_tec.visita_id FROM ( SELECT visita_id, UNNEST (visita.visita_tecnicos) AS tec_id FROM visita ) AS vis_id_tec LEFT JOIN tecnico ON vis_id_tec.tec_id = tecnico.tecnico_id GROUP BY vis_id_tec.visita_id )", 'algun_nombre', 'vis.visita_id = algun_nombre.visita_id')
                ->groupBy('vis.visita_id, vis.sucursal_id, suc.sucursal_nombre, emp.empresa_nombre, algun_nombre.nombre_tecnicos')
        ;
        $visits = $query->execute()->fetchAll();
        return $template->render('panelVisit.twig', ['BASE_URL' => BASE_FQDN . BASE_URL, 'visits' => $visits]);
    }

    public function ajaxGetBranches() {
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('*')
                ->from('sucursal')
        ;
        header('Content-type: application/json; charset=utf-8');
        return json_encode($query->execute()->fetchAll());
    }

    public function ajaxGetTechnicians() {
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('*', "concat(malbec.tecnico.tecnico_nombre, ' ', malbec.tecnico.tecnico_apellido) as nombre_completo")
                ->from('tecnico')
        ;
        header('Content-type: application/json; charset=utf-8');
        return json_encode($query->execute()->fetchAll());
    }

    public function ajaxCreateVisit() {
        $visita_id = date("dmYHisL");
        $sucursal_id = filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $visita_fecha = filter_input(INPUT_POST, 'visita_fecha', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);
        $technicians_list = filter_var_array($_POST['technicians_list'], FILTER_SANITIZE_NUMBER_INT);
        $array = array();
        foreach ($technicians_list as $technician) {
            $array[] = $technician['tecnico_id'];
        }
        $string = "{{" . implode("},{", $array) . "}}";

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.visita')
                ->values(array(
                    'visita_id' => ':visita_id',
                    'sucursal_id' => ':sucursal_id',
                    'visita_fecha' => ':visita_fecha',
                    'visita_tecnicos' => ':visita_tecnicos'
                ))
                ->setParameter('visita_id', $visita_id)
                ->setParameter('sucursal_id', $sucursal_id)
                ->setParameter('visita_fecha', $visita_fecha)
                ->setParameter('visita_tecnicos', $string)
        ;
        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(404);
        }
    }

    public function ajaxDeleteVisit() {
        $visita_id = filter_input(INPUT_POST, 'visita_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]);

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->delete('malbec.visita')
                ->where('visita_id = :visita_id')
                ->setParameter('visita_id', $visita_id)
        ;
        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(404);
        }
    }

}
