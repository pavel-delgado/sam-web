<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of apiController
 *
 * @author pdelgado
 */
class APIController {

    function apiLogin() {
        $user = trim(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $pass = trim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('tecnico_id', 'tecnico_nombre', 'tecnico_apellido')
                ->from('malbec.tecnico')
                ->where('tecnico_id=?')
                ->andWhere('tecnico_contrasena=?')
                ->setParameter(0, $user)
                ->setParameter(1, sha1($pass))
        ;
        $execquery = $query->execute();
        $response = array();
        if ($execquery->rowCount() > 0) {
            $response['response'] = true;
            $response['data'] = $execquery->fetch();
        } else {
            $response['response'] = false;
        }//nada
        return json_encode($response);
    }

    function apiVisit() {
        $user = trim(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $response = $database->executeQuery("select v.visita_id, s.sucursal_id, s.sucursal_nombre, v.visita_fecha, v.visita_estado from malbec.visita v, malbec.sucursal s where v.sucursal_id=s.sucursal_id and v.visita_estado=0 and ? = ANY(v.visita_tecnicos)", array($user))->fetchAll();
        return json_encode($response);
    }

    function apiEquipmentfromBranch() {
        $branch = trim(filter_input(INPUT_POST, 'sucursal_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('equipo_id', 'equipo_nombre', 'equipo_tipo', 'equipo_ubicacion', 'equipo_piso', 'equipo_marca', 'equipo_modelo', 'equipo_serie')
                ->from('malbec.equipo')
                ->where('sucursal_id=?')
                ->orderBy('equipo_piso', 'ASC')
                ->setParameter(0, $branch)
        ;
        $response = $query->execute()->fetchAll();
        return json_encode($response);
    }

    function apiRevisionfromEquipment() {
        $equipment = trim(filter_input(INPUT_POST, 'equipo_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('rev.equipo_id', 'rev.revision_tipo', 'rev.revision_notas', 'rev.visita_id', 'vis.visita_fecha')
                ->from('malbec.revision', 'rev')
                ->rightJoin('rev', 'malbec.visita', 'vis', 'rev.visita_id=vis.visita_id')
                ->where('rev.equipo_id=?')
                ->orderBy('vis.visita_fecha', 'DESC')
                ->setParameter(0, $equipment)
        ;
        $response = $query->execute()->fetchAll();
        return json_encode($response);
    }

    function apiRevisiontoEquipment() {
        $visita_id = trim(filter_input(INPUT_POST, 'visita_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $equipo_id = trim(filter_input(INPUT_POST, 'equipo_id', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $revision_tipo = trim(filter_input(INPUT_POST, 'revision_tipo', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $revision_notas = trim(filter_input(INPUT_POST, 'revision_notas', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));
        $revision_update = trim(filter_input(INPUT_POST, 'revision_update', FILTER_SANITIZE_STRING, [\FILTER_FLAG_STRIP_LOW, \FILTER_FLAG_STRIP_HIGH]));

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        if (($revision_update === "true")) {
            $query
                    ->update('malbec.revision')
                    ->set('revision_tipo', ':revision_tipo')
                    ->set('revision_notas', ':revision_notas')
                    ->where('visita_id = :visita_id')
                    ->andWhere('equipo_id = :equipo_id')
                    ->setParameter('visita_id', $visita_id)
                    ->setParameter('equipo_id', $equipo_id)
                    ->setParameter('revision_tipo', $revision_tipo)
                    ->setParameter('revision_notas', $revision_notas)

            ;
        } else {
            $query
                    ->insert('malbec.revision')
                    ->values(array(
                        'visita_id' => ':visita_id',
                        'equipo_id' => ':equipo_id',
                        'revision_tipo' => ':revision_tipo',
                        'revision_notas' => ':revision_notas'
                    ))
                    ->setParameter('visita_id', $visita_id)
                    ->setParameter('equipo_id', $equipo_id)
                    ->setParameter('revision_tipo', $revision_tipo)
                    ->setParameter('revision_notas', $revision_notas)
            ;
        }
        try {
            $query->execute();
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

}
