<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

class panelClientController {

    function pageClient() {
        global $template;
        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->select('emp.empresa_id', 'emp.empresa_nombre', 'count(suc.empresa_id) as nsucursales')
                ->from('empresa', 'emp')
                ->leftJoin('emp', 'sucursal', 'suc', 'emp.empresa_id = suc.empresa_id')
                ->groupBy('emp.empresa_id', 'emp.empresa_nombre')
        ;
        $clients = $query->execute()->fetchAll();
        return $template->render('panelClient.twig', ['BASE_URL' => BASE_FQDN . BASE_URL, 'clients' => $clients]);
    }

    public function ajaxCreateClient() {

        require __DIR__ . '/../backend/database.inc.php';

        $empresa_nombre = filter_input(INPUT_POST, 'empresa_nombre', FILTER_SANITIZE_STRING, [FILTER_FLAG_STRIP_LOW]);
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->insert('malbec.empresa')
                ->values(array(
                    'empresa_nombre' => ':empresa_nombre'
                ))
                ->setParameter('empresa_nombre', $empresa_nombre)
        ;

        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(409);
        }
    }

    public function ajaxDeleteClient() {
        $empresa_id = filter_input(INPUT_POST, 'empresa_id', FILTER_SANITIZE_NUMBER_INT);

        require __DIR__ . '/../backend/database.inc.php';
        $database = \Doctrine\DBAL\DriverManager::getConnection($databaseParams, $doctrineConfig);
        $query = $database->createQueryBuilder();
        $query
                ->delete('empresa')
                ->where('empresa_id = :empresa_id')
                ->setParameter('empresa_id', $empresa_id)
        ;
        try {
            $query->execute();
        } catch (\Exception $e) {
            http_response_code(404);
        }
    }

}
