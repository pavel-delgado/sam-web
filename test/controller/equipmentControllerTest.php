<?php

require_once '../controller/equipmentController.php';
require_once '../enviroment/env_dev.inc.php';
require_once '../vendor/autoload.php';

class equipmentControllerTest extends \PHPUnit_Framework_TestCase {
   
    
    public function testdeleteEquipment() {

        $controllerEquipment = new Controller\equipmentController();
        $response = $controllerEquipment->Delete_Equipment('123');
        $this->assertTrue($response != NULL, "as");
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testFailingInclude() {
        include 'not_existing_file.php';
    }

}
