<?php

/*
 *
 *  SAM
 */



require_once __DIR__ . '/vendor/autoload.php';

// MAIL
//$mail = new PHPMailer;
// ENVIROMENT
require_once __DIR__ . '/enviroment/env_dev.inc.php';
// require_once __DIR__ . '/enviroment/env_prod.inc.php';
// TEMPLATE
$twig_loader = new Twig_Loader_Filesystem(__DIR__ . '/templates/');
$template = new Twig_Environment($twig_loader, array());

// CONTROLLERS

foreach (glob(__DIR__ . "/controller/*.php") as $file) {
    require $file;
}

// ROUTES
use Phroute\Phroute\RouteCollector;

$router = new RouteCollector();
require __DIR__ . '/backend/router.inc.php';
$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());

// ROUTE + RENDER
if (preg_match('/\.(?:woff|woff2|ttf|map|js|css|png)[?]*[a-z-=0-9.@]*$/', $_SERVER["REQUEST_URI"])) {
    return false;
} else {

    try {
        echo $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    } catch (Phroute\Phroute\Exception\HttpRouteNotFoundException $e) {
        echo $template->render('baseLogin.twig', array('BASE_URL' => BASE_FQDN . BASE_URL));
    } catch (Phroute\Phroute\Exception\HttpMethodNotAllowedException $e) {
        echo $template->render('baseLogin.twig', array('BASE_URL' => BASE_FQDN . BASE_URL));
    }
}