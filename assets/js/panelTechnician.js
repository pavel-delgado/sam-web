/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var techniciansList = $('#technicians-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ Tecnicos)"
        },
        "columns": [
            {"width": "15%"},
            {"width": "25%"},
            {"width": "25%"},
            {"width": "25%"},
            {"width": "10%", "class": "dt-center", "orderable": false, "searchable": false}
        ]
    });

    // CREATE TECHNICIANS
    $('#newtechnician-submit-button').click(function (e) {

        if ($('#newtechnician-name-input').val() === "") {
            alert('Ingrese el nombre del Tecnico');
            return false;
        }
        if ($('#newtechnician-secondname-input').val() === "") {
            alert('Ingrese el apellido del Tecnico');
            return false;
        }
        if ($('#newtechnician-id-input').val() === "") {
            alert('Ingrese el RUT del Tecnico');
            return false;
        }
        if ($('#newtechnician-password-input').val() === "") {
            alert('Ingrese la contraseña para el Tecnico');
            return false;
        }
        //alert($('#newtechnician-rol-input').val()+"ok");
        $('#newtechnician-submit-button').prop('disabled', true);
        $.post(baseurl + '/ajax/tecnico/create', {
            tecnico_nombre: $('#newtechnician-name-input').val(),
            tecnico_apellido: $('#newtechnician-secondname-input').val(),
            tecnico_id: $('#newtechnician-id-input').val(),
            tecnico_contrasena: $('#newtechnician-password-input').val(),
            tecnico_rol: $('#newtechnician-rol-input').val(),
        })
                .done(function () {
                    alert('Tecnico añadido correctamente');
                    javascript:location.reload();
                })
                .fail(function () {
                    alert('Error al crear el Tecnico');
                    $('#newtechnician-submit-button').prop('disabled', false);
                });
    });

    // DELETE TECHNICIANS
    $('#technicians-list').on('click', '.technician-delete_action-button', function (e) {

        var tecnico_id = $(this).siblings('input').val();
        var tecnico_row = $(this).parents('tr');

        var respuesta = confirm("¿Eliminar Tecnico rut=" + tecnico_id + "?");

        if (respuesta) {
            $.post(baseurl + '/ajax/tecnico/delete', {
                tecnico_id: tecnico_id
            })
                    .done(function () {
                        techniciansList.row(tecnico_row).remove().draw();
                    })
                    .fail(function () {
                        alert('Error  al eliminar');
                    });
        }
    });


});