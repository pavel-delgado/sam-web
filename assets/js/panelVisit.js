/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var visitsList = $('#visits-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "order": [[1, "desc"]],
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ Visitas)"
        },
        "columns": [
            {"width": "25%", "className": "dt-center"},
            {"width": "15%", "className": "dt-center"},
            {"width": "15%", "className": "dt-center"},
            {"width": "20%", "className": "dt-center"},
            {"width": "15%", "className": "dt-center"},
            {"width": "10%", "className": "dt-center", "orderable": false, "searchable": false}
        ]
    });

    var technicians_list = $('#technicians-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "searching": false,
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ tecnicos)"
        },
        "columns": [
            {"orderable": false, "className": "dt-center"},
            {"className": "dt-center"},
            {},
            {"className": "dt-center"},
            {"className": "dt-center"}
        ]
    });

    $("#newvisit-branch-id-input").easyAutocomplete({
        url: baseurl + "/ajax/visita/branches",
        getValue: "sucursal_id",
        ajaxSettings: {
            dataType: "json",
            method: "POST"
        },
        placeholder: "Buscar sucursal mediante el rut",
        list: {
            match: {
                enabled: true
            }
        },
        template: {
            type: "description",
            fields: {
                description: "sucursal_nombre"
            }
        }
    });

    $("#newvisit-technician-input").easyAutocomplete({
        url: baseurl + "/ajax/visita/technicians",
        getValue: "tecnico_id",
        ajaxSettings: {
            dataType: "json",
            method: "POST"
        },
        placeholder: "Buscar técnico mediante el RUT",
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function (item) {
                switch ($("#newvisit-technician-input").getSelectedItemData().tecnico_rol) {
                    case 0:
                        var rol = "AYUDANTE";
                        break;
                    case 1:
                        var rol = "TÉCNICO MANTENEDOR";
                        break;
                    case 2:
                        var rol = "TÉCNICO AVANZADO";
                        break;
                }
                technicians.addTechnician(
                        $("#newvisit-technician-input").getSelectedItemData().tecnico_id,
                        $("#newvisit-technician-input").getSelectedItemData().tecnico_nombre,
                        $("#newvisit-technician-input").getSelectedItemData().tecnico_apellido,
                        rol
                        );
                $("#newvisit-technician-input").val('');
            }
        },
        template: {
            type: "description",
            fields: {
                description: "nombre_completo"
            }
        }
    });

    function technician(tecnico_id) {
        this.tecnico_id = tecnico_id;
    }

    var technicians = {
        technicians_list: [],
        addTechnician: function (tecnico_id, tecnico_nombre, tecnico_apellido, tecnico_rol) {
            // FIND DUPLICATE
            if (this.technicians_list.map(function (e) {
                return e.tecnico_id;
            }).indexOf(tecnico_id) > -1) {
                alert('El tecnico ya está incluido en la nueva visita.');
                return false;
            }
            // PUSH TO INDEX
            this.technicians_list.push(new technician(tecnico_id));
            // SHOW IN TABLE
            technicians_list.row.add([tecnico_id, tecnico_nombre, tecnico_apellido, tecnico_rol, '<button tecnico_id="' + tecnico_id + '" type="button" class="btn btn-sm btn-danger technician_delete-button"><i class="entypo-cancel"></i></button>']).draw();
        },
        removeTechnician: function (tecnico_id) {
            var index = this.technicians_list.map(function (e) {
                return e.tecnico_id;
            }).indexOf(tecnico_id);
            this.technicians_list.splice(index, 1);
        }
    }

    $('#technicians-list').on("click", ".technician_delete-button", function () {
        technicians_list.row($(this).parents('tr')).remove().draw();
        technicians.removeTechnician($(this).attr('tecnico_id'));
    });

    // CREATE VISIT
    $('#newvisit-submit-button').click(function (e) {

        if ($('#newvisit-id-input').val() === "") {
            alert('Ingrese el nombre de la sucursal');
            return false;
        }
        if ($('#newvisit-date-input').val() === "") {
            alert('Ingrese la fecha de la visita');
            return false;
        }
        if (technicians.technicians_list.length < 1) {
            alert('Debe ingresar al menos un tecnico para la visita');
            return false;
        }

        $('#newvisit-submit-button').prop('disabled', true);
        $.post(baseurl + '/ajax/visita/create', {
            sucursal_id: $('#newvisit-branch-id-input').val(),
            visita_fecha: $('#newvisit-date-input').val(),
            technicians_list: technicians.technicians_list
        })
                .done(function (e) {
                    alert('Visita añadida correctamente' + e);
                    javascript:location.reload();
                })
                .fail(function () {
                    alert('Error al crear la Visita');
                    $('#newvisit-submit-button').prop('disabled', false);
                });
    });

    // DELETE VISIT
    $('#visits-list').on('click', '.visita-delete_action-button', function (e) {

        var visita_id = $(this).siblings('input').val();
        var visita_row = $(this).parents('tr');

        var respuesta = confirm("¿Eliminar Visita?");

        if (respuesta) {
            $.post(baseurl + '/ajax/visita/delete', {visita_id: visita_id})
                    .done(function () {
                        visitsList.row(visita_row).remove().draw();
                    })
                    .fail(function () {
                        alert('Error  al eliminar');
                    });
        }
    });

    $('.toggle-visit-state').change(function() {
      console.log($(this).data('id'));
      
    });
});