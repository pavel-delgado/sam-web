/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var revisionsList = $('#revisions-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "order": [[1, "desc"]],
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ revisiones)"
        },
        "columns": [
            {"width": "15%"},
            {"width": "20%"},
            {"width": "20%"},
            {"width": "15%"},
            {"width": "30%", "orderable": false, "searchable": false}
        ]
    });


});