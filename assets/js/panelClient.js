/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var companiesList = $('#companies-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ clientes)"
        },
        "columns": [
            {"width": "20%", "className": "dt-center"},
            {"width": "25%", "className": "dt-center"},
            {"width": "20%", "className": "dt-center"},
            {"width": "25%", "className": "dt-center", "orderable": false, "searchable": false}
        ]
    });

    // CREATE CLIENT
    $('#newclient-submit-button').click(function (e) {

        if ($('#newclient-name-input').val() === "") {
            alert('Ingrese el nombre del cliente');
            return false;
        }

        $('#newclient-submit-button').prop('disabled', true);
        $.post(baseurl + '/ajax/cliente/create', {
            empresa_nombre: $('#newclient-name-input').val()
        })
                .done(function () {
                    alert('Cliente añadido correctamente');
                    javascript:location.reload();
                })
                .fail(function () {
                    alert('Error al crear el cliente');
                    $('#newquotation-submit-button').prop('disabled', false);
                });
    });

    // DELETE CLIENT
    $('#companies-list').on('click', '.cliente-delete_action-button', function (e) {

        var empresa_id = $(this).siblings('input').val();
        var empresa_row = $(this).parents('tr');

        var respuesta = confirm("¿Eliminar cliente?");

        if (respuesta) {
            $.post(baseurl + '/ajax/cliente/delete', {empresa_id: empresa_id})
                    .done(function () {
                        companiesList.row(empresa_row).remove().draw();
                    })
                    .fail(function () {
                        alert('Error  al eliminar');
                    });
        }
    });
});