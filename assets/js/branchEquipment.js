/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var equipmentList = $('#equipment-list').DataTable({
    "autoWidth": false,
    "paging": false,
    "info": false,
    "order": [[2, "desc"]],
    "language": {
        "search": "Filtrar: ",
        "lengthMenu": "Display _MENU_ records per page",
        "zeroRecords": "<b>Nada para mostrar</b>",
        "info": "",
        "infoEmpty": "Sin resultados",
        "infoFiltered": "(filtrando de un total de _MAX_ equipos)"
    },
    "columns": [
        {"width": "15%", "className": "dt-left"},
        {"width": "15%", "className": "dt-left"},
        {"width": "5%", "className": "dt-center"},
        {"width": "10%", "className": "dt-center"},
        {"width": "20%", "className": "dt-center"},
        {"width": "20%", "className": "dt-center"},
        {"width": "15%", "className": "dt-center", "orderable": false, "searchable": false}
    ]});

// CREATE EQUIPMENT
$('#newequipment-submit-button').click(function (e) {

    if ($('#newequipment-name-input').val() === "") {
        alert('Ingrese el nombre del equipo');
        return false;
    }
    if ($('#newequipment-type-input').val() === "") {
        alert('Ingrese el tipo del equipo');
        return false;
    }
    if ($('#newequipment-brand-input').val() === "") {
        alert('Ingrese la marca del equipo');
        return false;
    }
    if ($('#newequipment-model-input').val() === "") {
        alert('Ingrese el modelo del equipo');
        return false;
    }
    if ($('#newequipment-serial-input').val() === "") {
        alert('Ingrese el n° de serie del equipo');
        return false;
    }
    /*
     if ($('#newequipment-location-input').val() === "") {
     alert('Ingrese la ubicacion del equipo');
     return false;
     }
     */
    if ($('#newequipment-floor-input').val() === "") {
        alert('Ingrese el piso del equipo');
        return false;
    }
    if ($('#newequipment-periodicity-input').val() === "") {
        alert('Ingrese la periodicidad del equipo');
        return false;
    }
    //alert("suc:"+ $('#newequipment-branchid-input').val());
    $('#newequipment-submit-button').prop('disabled', true);
    $.post(baseurl + '/ajax/equipo/create', {
        sucursal_id: $('#newequipment-branchid-input').val(),
        equipo_nombre: $('#newequipment-name-input').val(),
        equipo_tipo: $('#newequipment-type-input').val(),
        equipo_marca: $('#newequipment-brand-input').val(),
        equipo_modelo: $('#newequipment-model-input').val(),
        equipo_serie: $('#newequipment-serial-input').val(),
        equipo_ubicacion: $('#newequipment-location-input').val(),
        equipo_piso: $('#newequipment-floor-input').val(),
        equipo_periodicidad: $('#newequipment-periodicity-input').val()
    })
            .done(function () {
                alert('Equipo añadido correctamente');
                javascript:location.reload();
            })
            .fail(function () {
                alert('Error al crear el Equipo');
                $('#newquotation-submit-button').prop('disabled', false);
            });
});

// DELETE EQUIPMENT
$('#equipment-list').on('click', '.equipment-delete_action-button', function (e) {

    var equipo_id = $(this).siblings('input').val();
    var equipo_row = $(this).parents('tr');

    var respuesta = confirm("¿Eliminar equipo?");

    if (respuesta) {
        $.post(baseurl + '/ajax/equipo/delete', {equipo_id: equipo_id})
                .done(function () {
                    alert("Equipo con ID " + equipo_id + " eliminado")
                    equipmentList.row(equipo_row).remove().draw();
                })
                .fail(function () {
                    alert('Error  al eliminar');
                });

    }
});

//MODAL DETAILS
$('body').on('click', '.details-modal', function () {
    // PARSE + PARSE JSON
    var detalles_json = $.parseJSON($.parseJSON(($(this).data('json'))));
    // CLEAR TABLE
    $('#equipment-details tbody').html('');
    // ADD TO TABLE
    for (var prop in detalles_json) {
        $('#equipment-details tbody').append('<tr><td>' + prop + '</td><td>' + detalles_json[prop] + '</td></tr>');
    }
    // SHOW
    jQuery('#modal-details').modal('show');


});