/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var branchesList = $('#branches-list').DataTable({
        "autoWidth": false,
        "paging": false,
        "info": false,
        "order": [[1, "asc"]],
        "language": {
            "search": "Filtrar: ",
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "<b>Nada para mostrar</b>",
            "info": "",
            "infoEmpty": "Sin resultados",
            "infoFiltered": "(filtrando de un total de _MAX_ votantes)"
        },
        "columns": [
            {"width": "10%"},
            {"width": "25%"},
            {"width": "15%"},
            {"width": "30%"},
            {"width": "20%", "className": "dt-center", "orderable": false, "searchable": false}
        ]
    });

    // CREATE BRANCH
    $('#newbranch-submit-button').click(function (e) {

        if ($('#newbranch-id-input').val() === "") {
            alert('Ingrese el id de sucursal');
            return false;
        }
        if ($('#newbranch-name-input').val() === "") {
            alert('Ingrese el nombre de sucursal');
            return false;
        }
        if ($('#newbranch-location-input').val() === "") {
            alert('Ingrese la comuna de la sucursal');
            return false;
        }
        if ($('#newbranch-address-input').val() === "") {
            alert('Ingrese la dirección de la sucursal');
            return false;
        }

        $('#newbranch-submit-button').prop('disabled', true);
        $.post(baseurl + '/ajax/sucursal/create', {
            sucursal_id: $('#newbranch-id-input').val(),
            empresa_id: $('#newbranch-clientid-input').val(),
            sucursal_nombre: $('#newbranch-name-input').val(),
            sucursal_comuna: $('#newbranch-location-input').val(),
            sucursal_direccion: $('#newbranch-address-input').val()
        })
                .done(function () {
                    alert('Sucursal añadida correctamente');
                    javascript:location.reload();
                })
                .fail(function () {
                    alert('Error al crear la sucursal');
                    $('#newquotation-submit-button').prop('disabled', false);
                });
    });

    // DELETE BRANCH
    $('#branches-list').on('click', '.cliente-delete_action-button', function (e) {

        var sucursal_id = $(this).siblings('input.branch_id').val();
        var empresa_row = $(this).parents('tr');


        var respuesta = confirm("¿Eliminar sucursal?");

        if (respuesta) {
            $.post(baseurl + '/ajax/sucursal/delete', {sucursal_id: sucursal_id})
                    .done(function () {
                        alert("sucursal " + sucursal_id + " eliminada");
                        branchesList.row(empresa_row).remove().draw();

                    })
                    .fail(function () {
                        alert('Error  al eliminar');
                    });

        }
    });

    //MODAL ENCARGADOS
    $('#branches-list').on('click', '.encargados-modal', function () {
        jQuery('#modal-encargados').modal('show');
        var sucursal_id = $(this).siblings('input.branch_id').val();
        $.post(baseurl + '/ajax/sucursal/encargados', {sucursal_id: sucursal_id})
                .done(function (json) {
                    var list = $('#tabla').DataTable({
                        "autoWidth": false,
                        "destroy": true,
                        "processing": true,
                        "data": JSON.parse(json),
                        "paging": false,
                        "searching": false,
                        "info": false,
                        "order": [[1, "asc"]],
                        "language": {
                            "search": "Filtrar: ",
                            "lengthMenu": "Display _MENU_ records per page",
                            "zeroRecords": "<b>Nada para mostrar</b>",
                            "info": "",
                            "infoEmpty": "Sin resultados",
                            "infoFiltered": "(filtrando de un total de _MAX_ encargados)"
                        },
                        "columns": [
                            {"width": "25%", "className": "dt-center", "data": "encargados"},
                            {"width": "25%", "className": "dt-center", "data": "encargado_nombre"},
                            {"width": "25%", "className": "dt-center", "data": "encargado_apellido"},
                            {"width": "25%", "className": "dt-center", "data": "encargado_tipo", "render": function (data, type, full, meta) {
                                    var tipo = "";
                                    switch (data) {
                                        case 0:
                                            tipo = "JEFE DE LOCAL"
                                            break;
                                        case 1:
                                            tipo = "JEFE ZONAL";
                                            break;
                                    }
                                    return tipo;
                                }
                            }
                        ]
                    });
                })
                .fail(function () {
                    alert('Error al cargar encargados');
                }, "json");
    });

    //CREATE ENCARGADO
    $('#newattendant-submit-button').click(function (e) {

        if ($('#newattendant-rut-input').val() === "") {
            alert('Ingrese el rut del encargado');
            return false;
        }
        if ($('#newattendant-email-input').val() === "") {
            alert('Ingrese el email del encargado');
            return false;
        }
        if ($('#newattendant-telefono-input').val() === "") {
            alert('Ingrese el telefono encargado');
            return false;
        }
        if ($('#newattendant-tipo-input').val() === "") {
            alert('Ingrese el tipo de encargado');
            return false;
        }
        if ($('#newattendant-nombre-input').val() === "") {
            alert('Ingrese nombre del encargado');
            return false;
        }
        if ($('#newattendant-apellido-input').val() === "") {
            alert('Ingrese apellido del encargado');
            return false;
        }

        $('#newattendant-submit-button').prop('disabled', true);
        $.post(baseurl + '/ajax/sucursal/createEncargado', {
            encargado_id: $('#newattendant-rut-input').val(),
            encargado_email: $('#newattendant-email-input').val(),
            encargado_telefono: $('#newattendant-telefono-input').val(),
            encargado_tipo: $('#newattendant-tipo-input').val(),
            encargado_nombre: $('#newattendant-nombre-input').val(),
            encargado_apellido: $('#newattendant-apellido-input').val()
        })
                .done(function () {
                    alert('Encargado añadido correctamente');
                    javascript:location.reload();
                })
                .fail(function () {
                    alert('Error al crear encargado');
                    $('#newquotation-submit-button').prop('disabled', false);
                });
    });

});

